#include <stdio.h>
#include <stdlib.h>

#define RED   1
#define BLACK 0

//AINDA INCOMPLETO!!!!

//Implementar inser��o e remo��o

typedef struct arvoreRB
{
    int info;
    int cor;
    struct arvoreRB *esq;
    struct arvoreRB *dir;
} ArvoreRB;

int eh_no_vermelho(ArvoreRB * no)
{
    if(!no) return BLACK;
    return(no->cor == RED);
}

int buscar (ArvoreRB *a, int v)
{
    if (a == NULL)
    {
        return 0;    /*Nao achou*/
    }
    else if (v < a->info)
    {
        return buscar (a->esq, v);
    }
    else if (v > a->info)
    {
        return buscar (a->dir, v);
    }
    else
    {
        return 1;    /*Achou*/
    }
}

void in_order(ArvoreRB *a)
{
    if(!a)
        return;
    in_order(a->esq);
    printf("%d ",a->info);
    in_order(a->dir);
}

void print(ArvoreRB * a,int spaces)
{
    int i;
    for(i=0; i<spaces; i++) printf(" ");
    if(!a)
    {
        printf("//\n");
        return;
    }

    printf("%d\n", a->info);
    print(a->esq,spaces+2);
    print(a->dir,spaces+2);
}

ArvoreRB * rot_esq(ArvoreRB * no)
{
    printf("\n COMECOU");
    printf("\n valor no->info: %d", no->info);
    printf("\n valor no->dir: %d", no->dir);
    ArvoreRB * x = no->dir;
    printf("\n valor x->info: %d", x->info);
    no->dir = x->esq;
    x->esq = no;
    x->cor = no->cor;
    no->cor = RED;
    printf("\n ROTACIONOIU");
    return (x);
}

ArvoreRB * rot_dir(ArvoreRB * no)
{
    ArvoreRB * x = no->esq;
    no->esq = x->dir;
    x->dir = no;
    x->cor = no->cor;
    no->cor = RED;
    return (x);
}

ArvoreRB * flip_cor(ArvoreRB * no)
{
    no->cor = RED;
    no->esq->cor = BLACK;
    no->dir->cor = BLACK;
    return(no);
}


ArvoreRB* inserir(ArvoreRB *a, int v, char lado, ArvoreRB *anterior)
{
    printf("\n comecou funcao inserir");
    char lad;

    if (a == NULL)
    {

        printf("\n entrou no if\n");
        a = (ArvoreRB*)malloc(sizeof(ArvoreRB));
        printf("\n alocou\n ");
        a->info = v;
        a->cor = RED;
        a->esq = a->dir = NULL;
        printf("\n info: %d \n cor: %d,\n lado: %c \n ", a->info, a->cor, lado);
        printf("\n return a: %p", &a);



    }
    else if (v < a->info)
    {
        printf("\n else\n");
        lad = 'e';
        a->esq = inserir (a->esq, v, lad, a);
        printf("\n foi para esquerda \n");
    }
    else
    {
        printf("\n else22\n");
        lad = 'd';
        a->dir = inserir (a->dir, v, lad, a);
        printf("\n FOI PARA A DIREITA \n");
    }
    //printf("\n return a: %p", a->info);

    return a;
}



int main()
{

    printf("%d", sizeof(ArvoreRB));
    ArvoreRB * a;
    printf("\n \n\n INSERINDO 5 \n");
    a = inserir(NULL, 5,'j',NULL);
    printf("\n \n\n INSERINDO 30 \n");
    a = inserir(a,30,'j',NULL);

    printf("\n \n\n INSERINDO 90 \n");
    a = inserir(a,90,'j',NULL);
    printf("\n \n\n INSERINDO 20 \n");
    a = inserir(a,20,'j',NULL);
    printf("\n \n\n INSERINDO 40 \n");
    a = inserir(a,40,'j',NULL);
    printf("\n \n\n INSERINDO 95 \n");
    a = inserir(a,95,'j',NULL);
    printf("\n \n\n INSERINDO 10 \n");
    a = inserir(a,10,'j',NULL);
    printf("\n \n\n INSERINDO 35 \n");
    a = inserir(a,35,'j',NULL);
    printf("\n \n\n INSERINDO 45 \n");
    a = inserir(a,45,'j',NULL);
    printf("\n \n\n INSERINDO 37 \n");
    a = inserir(a,37,'j',NULL);

    printf("\n");
    print(a,20);
    printf("\n");

    return 0;

}
